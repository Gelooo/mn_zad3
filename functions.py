import numpy as np
import sympy as sp


#  funckja zwracająca wartość podanej funkcji dla danego argumentu x
def funtions_value(x, function_number):
    # function_number = 1 -> 4x-3
    # function_number = 2 -> 2 * |x| - 1
    # function_number = 3 -> 0.5 * x^3 - 4 * x^2 + 2 * x
    # function_number = 4 -> 3x * sin(x) - 2x * cos(2x)
    # function_number = 5 -> -0.69 * x^2 + 1.5^x - 6 * sin(2x) + 6.9
    if function_number == 1:
        value = 4 * x - 3
    elif function_number == 2:
        value = 2 * np.abs(x) - 1
    elif function_number == 3:
        value = horner([0.5, -4, 2, 0], x)
    elif function_number == 4:
        value = 3 * x * np.sin(x) - 2 * x * np.cos(2 * x)
    elif function_number == 5:
        value = -0.69 * x ** 2 + 1.5 ** x - 6 * np.sin(2*x) + 6.9

    else:
        value = None
    return value


def horner(factors, x):
    result = factors[0]
    for a in range(1, len(factors)):
        result = result * x + factors[a]
    return result
