
def lagrangeInterpoaltion(args, values, x):
    y = 0.0
    for i in range(len(args)):
        t = 1.0
        for j in range(len(args)):
            if j != i:
                t = t * ((x - args[j]) / (args[i] - args[j]))
        y += t * values[i]
    return y