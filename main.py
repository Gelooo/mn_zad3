import numpy as np
from functions import funtions_value
from plot import show_plot
from lagrange import lagrangeInterpoaltion

# =========================== #
#   Grzegorz Kucharski 229932 #
#   Wojciech Cwynar           #
#   Grupa 5                   #
# =========================== #

print("Metoda interpolacji lagrange'a dla nierownych odstepow argumentow")
print("Położenia węzłów (plik args.txt):")

args = None
try:
    args = np.genfromtxt("args.txt", delimiter=" ")
    print(args)
except OSError:
    print("Nie udalo sie wczytac pliku")
    exit(0)

while True:
    correct = False
    function_number = 0  # numer wybranej funkcji
    while correct is not True:
        print("""\nWybierz numer funkcji:
        1 - liniowa: 4x-3
        2 - 2|x| - 1
        3 - wielomianowa: 0.5 * x^3 - 4 * x^2 + 2 * x
        4 - trygonometryczna: 3x * sin(x) - 2x * cos(2x)
        5 - złożona: -0.69 * x^2 + 1.5^x - 6 * sin(2x) + 6.9""")
        try:
            function_number = int(input())
            if 0 < function_number < 6:
                correct = True
            else:
                print("Bledny numer funkcji")
                correct = False
        except ValueError:
            print("Bledny numer funkcji")

    list = []
    for i in range(len(args)):
        list.append(funtions_value(args[i], function_number))

    values = np.array(list)
    left = args[0]
    right = args[len(args) - 1]

    show_plot(left, right, function_number, args, values)
