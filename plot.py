import matplotlib.pyplot as mp
import numpy as np
from functions import funtions_value
from lagrange import lagrangeInterpoaltion


def show_plot(left, right, funtion_number, args, values):
    x = np.linspace(left, right, 1000)  # oś OX od lewego do prawego przedziału, 1000 wartości
    mp.plot(x, funtions_value(x, funtion_number), label="Funkcja podstawowa")  # rysowanie wykresu
    mp.plot(x, lagrangeInterpoaltion(args, values, x), label="Funkcja po interpolacji")  # rysowanie wykresu
    for i in range(len(args)):
        mp.plot(args[i], values[i], 'X')
    mp.title("Interpolacja Lagrange'a")
    mp.xlim(left, right)
    mp.ylabel("f(x)")  # nazwa osi OY
    mp.xlabel("x")  # nazwa osi OX
    mp.legend(loc="upper left")
    mp.grid(True)  # siatka
    # legenda wykresu

    mp.show()
